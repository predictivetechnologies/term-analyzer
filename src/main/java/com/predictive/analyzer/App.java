package com.predictive.analyzer;

import java.io.*;

import java.util.*;

import java.util.regex.*;

import org.apache.lucene.document.*;

import org.apache.lucene.index.*;

import org.apache.lucene.store.*;

import org.apache.lucene.util.*;

public class App {

    public static class EntropyToken implements Comparable<EntropyToken> {
        private Double entropy;
        private Double probabilitySum;
        private String token;

        public EntropyToken(String token, Double entropy, Double probabilitySum) {
            this.entropy = entropy;
            this.token = token;
            this.probabilitySum = probabilitySum;
        }

        public String getToken() {
            return token;
        }

        public double getEntropy() {
            return entropy;
        }

        public double getProbabilitySum() {
            return probabilitySum;
        }

        public int compareTo(EntropyToken other) {
            return entropy.compareTo(other.entropy);
        }

        public String toString() {
            return token + "|" + entropy + "|" + probabilitySum;
        }

    }

    public static String findPattern( List<Pattern> patterns, String url) {
        for(Pattern pattern : patterns) {
            Matcher matcher = pattern.matcher(url);
            if(matcher.matches()) {
                return pattern.pattern();
            }
        }
        return null;
    }

    public static void main( String[] args ) throws Exception {

        Set<String> tokens = new HashSet<>();
        List<EntropyToken> entropies = new ArrayList<>();
        Map<String, Long> totalTermCount = new HashMap<>();
        Map<String, Map<String, Long>> termCount = new HashMap<>();

        List<Pattern> patterns = new ArrayList<>();

        String indexPath = args[0];

        Directory directory = new NIOFSDirectory(new File(indexPath));
        DirectoryReader reader = DirectoryReader.open(directory);

        Fields fields = MultiFields.getFields(reader);

        System.out.println("Found fields");
        for(String field : fields) {
            System.out.println(field);
        }



        if(args.length > 1) {
            String field = args[1];
            File outputFile = new File(args[2]);
            String regexFile = args[3];

            try(BufferedReader regexReader = new BufferedReader(new FileReader(regexFile))) {
                String line;
                while((line = regexReader.readLine()) != null) {
                    System.out.println(line);
                    Pattern p = Pattern.compile(line);
                    patterns.add(p);
                }
            }

            try(BufferedWriter output = new BufferedWriter(new FileWriter(outputFile));
                    PrintWriter writer = new PrintWriter(output)) {
                Terms fullTerms = MultiFields.getTerms(reader, field);

                System.out.printf("Found %d terms in %d docs\n", fullTerms.size(), reader.numDocs());

                Bits liveDocs = MultiFields.getLiveDocs(reader);

                for (int i=0; i < reader.maxDoc(); i++) {
                    if (liveDocs != null && !liveDocs.get(i)) {
                        continue;
                    }

                    Document doc = reader.document(i);
                    Terms terms = reader.getTermVector(i, field);
                    String url = doc.get("url");
                    String pattern = findPattern(patterns, url);

                    TermsEnum termsEnum = terms.iterator(null);

                    if(!totalTermCount.containsKey(pattern)) {
                        totalTermCount.put(pattern, 1L);
                        termCount.put(pattern, new HashMap<String, Long>());
                    } else {
                        totalTermCount.put(pattern, totalTermCount.get(pattern) + 1L);
                    }

                    while(termsEnum.next() != null) {
                        String termText = termsEnum.term().utf8ToString();
                        tokens.add(termText);
                        Map<String, Long> siteTokens = termCount.get(pattern);
                        if(!siteTokens.containsKey(termText)) {
                            siteTokens.put(termText, 1L);
                        } else {
                            siteTokens.put(termText, siteTokens.get(termText) + 1L);
                        }
                    }


                }

                System.out.printf("Processed %d tokens\n", tokens.size());

                System.out.println("Number of documents found by site");
                for(String key : totalTermCount.keySet()) {
                    System.out.println(key + " " + totalTermCount.get(key));
                }

                System.out.println("Calculating entropy");
                for(String token : tokens) {
                    double totalEntropy = 0;
                    List<Double> probs = new ArrayList<Double>();
                    double probNormalizer = 0.0;

                    for(String site : totalTermCount.keySet()) {
                        Map<String, Long> siteTokens = termCount.get(site);
                        Long tokenFreq = siteTokens.get(token);
                        Long totalWords = totalTermCount.get(site);

                        if(tokenFreq == null) {
                            continue;
                        }


                        double prob = (double)tokenFreq / (double)totalWords;
                        probs.add(prob);
                        probNormalizer += prob;
                    }

                    for(Double prob : probs) {
                        Double normProb = prob / probNormalizer;
                        double individualEntropy = -1 * normProb *Math.log(normProb);
                        totalEntropy += individualEntropy;
                    }

                    entropies.add(new EntropyToken(token, totalEntropy, probNormalizer / totalTermCount.size()));
                }
                Collections.sort(entropies);

                for(EntropyToken entropy : entropies) {
                    writer.println(entropy);
                }
            }

        }
    }
}
