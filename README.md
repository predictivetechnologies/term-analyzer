Terms Analyzer
===============

This is meant to compute entropies of terms in a lucene index


Building it
=============
You can compile like this:

mvn clean compile assembly:single


Running it
============
And you can run it like this:

java -jar target/term-analyzer-1.0-SNAPSHOT-jar-with-dependencies.jar <lucene-index> <field> <output-file> <regex-file>

The outpu-file file is separated by CSV

